SHORT_TITLE = FS
CHAPTERS := \
	Vorwort\
	Namen\
	Flammenfinger\
	Elemente\
	Zeitkick\
	Enttaeuschungen\
	Umplanen\
	Gemetzel\
	Planung\
	Sex\
	Besprechen\
	Wald\
	Fallen\
	Spionage\
	Vorlese\
	Vorlese-Planung
INPUTS := $(CHAPTERS:%=chapters/%.md) metadata.yaml
OUTPUTS := Flederschatten.pdf Flederschatten.epub Flederschatten.tex Flederschatten.html ContentNotes.tex $(SHORT_TITLE)/ContentNotes.md $(SHORT_TITLE)
SPBUCHSATZV := SPBuchsatz_Projekte_1_4_6
#SPBUCHSATZV := SPBuchsatz_Projekte_1_4__Pre_Beta_11
#SPBUCHSATZV := SPBuchsatz_Projekte_1_3

all: $(OUTPUTS)

Flederschatten.html: $(INPUTS)
	pandoc \
		$(INPUTS) \
		-o $@


Flederschatten.epub: $(INPUTS)
	pandoc \
		-t epub2\
		--css epub.css\
		$(INPUTS) \
		-o $@

Flederschatten.tex: $(INPUTS)
	~/.local/bin/pandoc \
		--top-level-division=chapter \
		-t SPBuchsatz.lua \
		$(INPUTS) \
		-o $@

ContentNotes.tex: chapters/ContentNotes.md
	~/.local/bin/pandoc \
		--top-level-division=chapter \
		-t SPBuchsatz.lua \
		$^ \
		-o $@


# changed Ifstr to ifstr in SPBuchsatz_System/Helferlein/Hilfsfunktionen.tex 
Flederschatten.pdf: Flederschatten.tex ContentNotes.tex TitleAuthor.tex
	cp Flederschatten.tex \
		deps/$(SPBUCHSATZV)/Buch/tex/Haupttext.tex
	cp ContentNotes.tex \
		deps/$(SPBUCHSATZV)/Buch/tex/ContentNotes.tex
	cp TitleAuthor.tex \
		deps/$(SPBUCHSATZV)/Buch/tex/TitleAuthor.tex
	cd deps/$(SPBUCHSATZV)/Buch/build/ ; \
		lualatex ../tex/Buch.tex
	cp deps/$(SPBUCHSATZV)/Buch/build/Buch.pdf Flederschatten.pdf

$(SHORT_TITLE)/titlelist.txt: Makefile
	mkdir -p $(shell dirname $@)
	@echo '$(CHAPTERS)' | xargs -n 1 | cat -n >$@

get_chapter_number = $(shell \
	grep -E '^[[:digit:][:space:]]+$(1:chapters/%.md=%)$$' $(SHORT_TITLE)/titlelist.txt | \
		awk '{ print $$1 }' \
)


chapters-epub/%.epub: chapters/%.md $(SHORT_TITLE)/titlelist.txt
	pandoc \
		-t epub2\
		--css epub.css\
		--metadata title="$(call get_chapter_number,$<) - $(shell \
			head -n 1 '$<' \
		)"\
		$< \
		-o $@

$(SHORT_TITLE)/%.md: chapters/%.md $(SHORT_TITLE)/titlelist.txt
	@echo '$@'
	@echo >$@ '---'
	@echo >>$@ 'layout: $(SHORT_TITLE)-chapters'
	@echo >>$@ 'page-category: story-chapters-$(SHORT_TITLE)'
	@echo >>$@ 'title: "$(call get_chapter_number,$<) - $(shell \
			head -n 1 '$<' \
		)"'
	@echo >>$@ 'number: $(call get_chapter_number,$<)'
	@echo >>$@ 'lang: de'
	@echo >>$@ '---'
	@echo >>$@ ''
	@cat $< >>$@

$(SHORT_TITLE)/ContentNotes.md: chapters/ContentNotes.md
	@echo '$@'
	@echo >$@ '---'
	@echo >>$@ 'layout: $(SHORT_TITLE)-chapters'
	@echo >>$@ 'page-category: story-chapters-$(SHORT_TITLE)'
	@echo >>$@ 'title: "Content-Notes"'
	@echo >>$@ 'number: 0'
	@echo >>$@ 'lang: de'
	@echo >>$@ '---'
	@echo >>$@ ''
	@cat $< >>$@

$(SHORT_TITLE): $(CHAPTERS:%=$(SHORT_TITLE)/%.md) ;

clean:
	rm -rf $(OUTPUTS)

.PHONY: clean $(SHORT_TITLE)
