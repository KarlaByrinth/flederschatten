Vorwort {.unnumbered}
=======

Dieses Buch steht unter Creative Commons Lizenz:

[http://creativecommons.org/licenses/by-nc-nd/4.0/](http://creativecommons.org/licenses/by-nc-nd/4.0/)


![image of the by-nc-nd 4.0 licence](/home/kaulquake/Fischtopf/Geschichten/Myrie/license.png){width=0.8 height=0.4}

Das Buch ist Ende des Sommers 2021 innerhalb von so etwa 4-8 Wochen entstanden.

Das Cover, ist mit gimp erstellt.

Ich behandele in wörtlicher Rede Punkte gleichberechtigt mit anderen Satzzeichen. Ich setze
mich damit über gängige Grammatik-Regeln hinweg, einfach, weil es mir so besser
gefällt und konsistenter vorkommt.
