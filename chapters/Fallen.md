Fallen
======

Flederschattens Schlafraum, in dem Mirash materialisierte, als
as wieder jointe, hatte zwei dreieckige Glasfronten an den
beiden Enden des Raums, die nicht Dachschrägen waren. Glas, wie es damals in
Fork verbaut gewesen sein mochte, Mirash kannte sich nicht so
sehr aus. Der Raum gehörte zu einer Wohnung am Rande Forks in den
oberen Lagen. Die Aussicht war atemberaubend beeindruckend. In der
einen Richtung erstreckte sich Fork in diesem düsteren, kalten
Stil. Irgendwo unter ihnen konnte Mirash sogar das Wasser sehen, das
die halbe Stadt bedeckte. Manchmal schwamm ein heller Fleck
durch den dunklen, stillen Strom, -- wohl eine Nixe
mit Unterwasserbeleuchtung, oder vielleicht der geschminkte
Hecht. Auf der anderen Seite blickte Mirash auf ein paar
wenige Felder hinaus, die sehr altertümlich bewirtschaftet
wirkten, obwohl sie wahrscheinlich gar nicht bewirtschaftet waren, und
dahinter erstreckte sich ein schwarzer, qualmender, explosiver
Himmel über eine dunkle Vulkanlandschaft. Mirash stand
hier staunend, konnte sich seit bestimmt
einer sechstel Stunde kaum davon losreißen und erblickte
nun den zweiten Kometen, der irgendwo weit entfernt auf die
Oberfläche des Planeten krachte. Nichts war zu hören. Aber das, das
war der Planet Arda simuliert zu einer Zeit, bevor er von
irgendetwas auch nur Maden-ähnlichem bewohnt gewesen war. Es war
wunderschön.

Mirash riss sich vom Anblick los. Leider war der Raum voll solcher
Anblicke. Es gab hier einen schlichten Dielenboden, eine brennende,
große Nachtkerze auf einem niedrigen Tisch, unter dem die Kröte
schlief, und ein niedriges Doppelbett, dass so unglaublich einladend
kuschelig wirkte. Auch diese Räumlichkeiten wirkten nicht so
luxuriös wie manches in der Zeit-Basis, aber vielleicht waren sie
es trotzdem. Es entsprach sehr Mirashs
Geschmack und as rechnete damit, dass Flederschatten
einfach einen ähnlichen hatte. Dass fer sich zusammengesammelt hatte, was
fiem nun einmal gefiel.

Es lagen verschiedene, weiche und weniger flauschige Decken im Bett, sowie
Flederschatten selbst auf den Bauch in einige davon halb gewickelt. Flederschatten
atmete ruhig. Mirash hätte fiem ein bisschen zur Seite drängen müssen, hätte
as sich dazu legen wollen. Aber das wollte as gar nicht. Trotzdem
blieb Mirashs Blick eine Weile an diesem Anblick hängen. Flederschatten
hatte den Zopf gelöst. Das lange, feste, dicke Haar lag im Bett
verteilt. Es sah insgesamt so einladend gemütlich und schön aus.

Mirash unterdrückte ein Seufzen und schlich leise die Treppe hinab ins
Stockwerk unter dem Schlafzimmer. Bevor as die Stufe nahm, bei der
sain Kopf unterhalb des Schlafzimmerbodens verschwinden würde, blickte
as noch einmal auf das Bett. As erwartete einen Augenblick, dass
die Gestalt nicht mehr dort liegen, sondern, sobald as sich
umdrehte, direkt vor ihm stehen würde, aber fer lag dort
unverändert.

Auch dieses Stockwerk hatte das Problem, sehr schön zu sein, aber
nicht Mirashs Ziel: Es war voller Bücher, und zwar zu einem großen
Teil selbstgeschriebener Bücher. Bei ihrer Mini-Führung, bevor Mirash
das Spiel verlassen hatte, hatte Flederschatten erklärt, dass fer
hier häufig gelehrt hatte. Einige Personen lernten gut, wenn Dinge
aufgemalt oder aufgeschrieben wurden, also hatte fer sich mit
ihnen hingesetzt und sie hatten in leere Bücher gekritzelt. Hier
hob Flederschatten die Bücher auf. Es waren vielleicht drei Regale
voll.

Das Stockwerk darunter war eines mit Zugang zu einer Brücke nach
draußen. Den hatte Flederschatten Mirash zeigen wollen, damit as
entscheiden könnte, Flederschatten zu verlassen.

Es gab außerdem eine Garderobe, eine Sitzgruppe und eine verschlossene
Tür, die Flederschatten nicht weiter erwähnt hatte. Sie hatte
ein Schloss, das sich mit Zeitmagie öffnen ließ und für Mirash nicht
zu schwierig wirkte. Trotzdem besah Mirash es noch einen Moment
eingehend, ob as irgendetwas übersehen
haben könnte, bevor as sich daran traute.

Mirash ließ es möglichst sachte aufspringnen, lauschte und sah sich noch
einmal gründlich um, bevor as
die Tür nur einen winzigen Spaltbreit
öffnete. Etwas fühlte sich seltsam an dabei. Die Tür wirkte schwerer, als
so eine Holztür sein sollte. Mirash blickte nach oben und entdeckte
einen schweren Gegenstand, der auf Türrahmen und Tür auflag. Mirash
versuchte zu erkennen, ob er herunterfallen würde, wenn as die Tür
weiter aufschieben würde. Aber as hatte keine Chance, außer
auszuprobieren. Mit einer Hand nach oben gestreckt, um den Gegenstand im
Falle des Herabfallens aufzufangen, schob Mirash sehr langsam die
Tür so weit auf, dass as sich hindurchquetschen konnte. Es
wirkte wackelig, aber nichts passierte.

Als Mirash im Raum an der Wand stand, blickte as noch einmal
zurück, ehe as die Tür hinter sich wieder vorsichtig schloss.
Mirash konnte nicht genau ausmachen, um was für einen
Gegenstand es sich handelte. Er lag nun wieder wie magisch
auf Türrahmen und Tür auf, als gehörte er genau dort hin.

In den Raum fiel fahles Licht von der Seite. Mirash blickte sich
um und erkannte zügig, dass es über ein paar optische Linsen
gebündelt zu zwei Stahlen wurde, die nur eine Fußlänge
vor ihm den Weg versperrten. Natürlich nicht versperrten,
Licht konnte durchschritten werden. Aber die kaum im
Staub ausmachbaren Lichtstrahlen trafen auf der anderen Seite
in zwei kleine Spiegel. Also vermutete Mirash, dass sie
eine Funktion hatten. Dass die Strahlen nicht durchbrochen
werden durften.

Immerhin befanden sie sich nur auf Knie und auf Knöchelhöhe.
Vielleicht, damit sie nicht auffielen. Mirash blickte sich weiter
um, um möglichst nichts zu übersehen und stieg, als as erst
einmal nichts weiter entdecken konnte, vorsichtig
über die Strahlen hinweg, ohne sie zu berühren. Nichts
explodierte. Mirash verharrte ruhig an der neuen
Position. Das konnte es nicht gewesen sein, oder?

Mirash erinnerte sich an die Fallen, die schon da gewesen
waren. Eine mit Zeit, eine mit Kraft, eine mit
Optik. Es fehlten Reibung und Transformation. War der Boden
besonders glatt? Er wirkte schon relativ glatt, aber
als Mirash vorsichtig mit dem Fuß darüber rieb, auch nicht
so glatt, dass as nicht hätte ohne große Vorsicht darübergehen
können. Er hatte ein feines Muster aus Sprüngen und Rillen, als
wäre er alt, als hätte das Haus schon damit gearbeitet.

Der Raum war insgesamt nicht sehr groß und nicht sehr
voll, aber ein bisschen Gerümpel stand herum. Ein Hutständer
etwa, ein Farbeimer, eine kleine Hebebühne, ein Besen und
ein Rollbrett. Mirash hatte sich seit der Vorstellung einige
Gedanken über Reibung gemacht und assoziierte das Rollbrett sofort
mit diesem Element. Zu den überigen Gegenständen hatte
Mirash keine direkte Assoziation, aber die Hebebühne wirkte
am extravagantesten. Was fehlte, war Transformation, -- das
konnte passen. Mirash nahm die Hebebühne genauer in Augenschein
und stellte fest, dass sie sich nur auf einer Seite anheben würde,
wenn as an der Kurbel kurbeln würde. Sie war schwer zu bewegen. Wenn
Mirash mit dem Rollbrett von ihr herunterrollen würde, während sie
gekippt war, würde as einfach in den übrigen Teil des Raums fahren. Etwas
rechts vom abgeschätzten Aufprallpunkt mit der Wand befand sich
ein Heizungsrohr, sonst war der Raum leer.

Mirash betrachte den Raum noch einmal gründlich, konnte sich aber
keinen besseren Reim auf alles machen. Auf die Gefahr hin, dass
irgendwas explodieren würde, legte sich Mirash vorsichtig auf das
Rollbrett, das as solange am oberen Rand der Hebebühne fixierte
und ließ los. As beschleunigte ziemlich schnell, aber der Boden
hatte eine überraschend hohe Reibung und auf der einen Seite mehr
davon als auf der anderen, sodass das Rollbrett kurz vor dem
Heizungsrohr zum Stehen kam. Mirash nahm das Rohr in Augenschein
und stellte fest, dass es keineswegs ein Heizungsrohr war.

<!--Und neben dem Rollbrett
lag ein riesiger Hufeisenmagnet. Reibung und Magnetismus
zusammen also, dachte Mirash. As versuchte, sich vorsichtig
auf das Rollbrett zu setzen, und nahm den Magnet in die
Hand. As hielt ihn einfach nach vorn. Irgendwovon wurde
er angezogen, wenn auch sehr wenig. Es brauchte sehr
lange, bis das Rollbrett sich beschleunigte, aber
dann wurde es fast unangenehm schnell. Mirash raste
einem Metallrohr an der Wand entgegen. As versuchte
mit dem Fuß abzubremsen, aber der Boden war eben durchaus
nicht wenig glatt. As bremste mutiger und war nur gerade so in der Lage,
die Fahrt ausreichend zu verlangsamen, dass as am Ende mit den
Füßen gegen die Wand gedrückt den Magneten davon abhalten konnte, ein
lautes Geräusch zu verursachen, als er sich mit dem Rohr
vereinigte.

Mirash fragte sich, ob as den Magneten direkt wieder lösen
und aufräumen sollte. Oder was das insgesamt hier sollte. Es
wirkte nicht so richtig wie eine Falle oder wie ein
Mechanismus, den Mirash verstünde.

As blickte sich ratlos um. Die Elemente waren durch. Es
gab nichts, was wie ein interessanter Ansatzpunkt zum
Weitermachen aussah. Und hinterher sollte aufgeräumt
sein. Also zog Mirash doch am Magneten. Und etwas
Überraschendes passierte: Das Heizungsrohr bewegte
sich.-->
Mirash rüttelte vorsichtig daran, und es ließ sich tatsächlich
bewegen. Mirash brauchte aber noch einige Momente, bis as auf
die Idee kam, es anzuheben, wodurch
sich ein Treppenschacht öffnete. Keine so beeindruckende
Wendeltreppe, wie in manchen Filmen mit Special Effects
für einen Geheimgang. Es hob sich dadurch einfach
ein Teil des Bodens etwas an, dessen Ränder sich eingermaßen
nahtlos in die Sprünge im Boden eingefügt hatten, und der eine Falltür
darstellte. Mirash öffnete sie vorsichtig ganz, blickte
sich noch einmal um und stieg die schmale Treppe
hinab.

As landete in einem Raum voller Kleidungsstücke. Sie
waren vorwiegend schwarz und passend zu Flederschattens
Körper. Einige hatten Schattierungen in dunkelgrau. Es war
sehr verschiedene Kleidung: Kurze und lange Kleider, Anzüge,
mehr Weiches wie Hoodies, -- die Mirash am liebsten befühlt
hätte, aber es sein ließ --, Mäntel, Kleidung, die fester
wirkte. Mirash schritt auf letztere zu. Bei der Vorbereitung
mit der Zeit-Fraktion auf so eine Situation hatte Mirash
grob gelernt, Rüstwert von Kleidung einzuschätzen. Aber eigentlich
hatte all die Kleidung keinen besonders hohen Rüstwert, glaubte
Mirash zu erkennen. Oft hatte eher Unterkleidung einen.

Die Zeit-Fraktion hatte auch erklärt, dass Personen hier grundsätzlich
verschiedene Garderoben hatten: Welche nur zum Ausgehen, für die
Dramaturge vor allem oder für Bälle, wenn welche stattfanden, welche
für Straßenkämpfe, wo weniger wertvolle Rüstung getragen wurde, welche für
Raids, oder auch welche für Kämpfe, bei denen davon ausgegangen wurde, dass
Personen nicht starben. Diese Garderobe hier sah aus wie eine nur
zum Ausgehen.

Trotzdem betastete Mirash doch vorsichtig Kleidung in der Ecke des
Raums, wo sie etwas fester wirkte.

"Dachte ich es mir doch." Flederschattens Stimme kam von neben
dem Treppenabgang.

Mirash blickte sich, möglichst gelassen wirkend um. "Dass ich
es doch hinkriegen würde, mich nicht in die Luft zu jagen?"

"Dass das ganze Motiv hinter der Sache, die du da abgezogen
hast, war, dass du bei mir in der Basis landest, um zu
spionieren oder zu sabotieren.", korrigierte Flederschatten.

Wow, das war mal präzise geschlossen. Mirash wurde wieder einen
Augenblick sehr heiß. As überlegte, ob rausreden eine sinnvolle
Strategie wäre, erst einmal unabhängig davon, ob as das überhaupt könnte. Aber
dann entschied sich Mirash doch dagegen. "Wie lang denkst du
dir das schon?"

"Ich würde sagen, von Anfang an.", sagte Flederschatten. "Aber
das ist nicht ganz richtig. Am Anfang war ich noch ein bisschen
zu sehr davon eingenommen, die Dramaturge zu retten, als das
ich sofort hätte klar denken können."

Mirash dachte darüber nach, ob das nicht eigentlich für
Flederschatten ein sehr offensichtlicher Schluss gewesen sein müsste. Aber
eigentlich hatte as da auch schon vorher drüber nachgedacht und
war zu dem Schluss gekommen, dass das nicht so klar wäre.

"Du wirkst noch unüberzeugt oder nachdenklich.", sagte
Flederschatten.

"Ich frage mich, warum das für dich so offensichtlich wirkt.", gab
Mirash zu.

"Nicht offensichtlich. Nur sehr wahrscheinlich.", korrigierte
Flederschatten. "Die Zeit-Fraktion kennt mich gut genug, dass
sie weiß, dass ich einschreite. Richtig?"

Mirash nickte. "Soweit klar. Ich hätte mich nicht darauf
eingelassen, irgendetwas in der Richtung zu tun, wenn nicht
sehr nahegelegen hätte, dass du die Dramaturge rettest."

Flederschatten grinste und verschränkte die Arme. Das
Haar war immer noch offen und leicht gewellt wegen des
Zopfs, der darin gewesen war. Fer trug ein schwarzes
Nachthemd mit schwarzglänzenden, aufgestickten Gespenstern. "Schon
witzig, dass euer Plan beinhaltet, dass das angebliche
Super-Villain rettet, was euch wichtig ist."

"Weil es dir eben auch wichtig ist.", argumentierte
Mirash.

"Schon." Flederschattens Grinsen wurde breiter. "Aber stell
dir das Mal in Real Life irgendwo historisch vor: Die Guten
stellen eine Falle, indem sie etwas kaputt zu machen drohen, was
ihnen lieb ist, damit die Bösen kommen und es retten. Etwas
kurios, nicht?"

Nun musste auch Mirash grinsen. "Schon irgendwie."

"Das klingt, als wolle ich es kritisieren, glaube ich." Flederschatten
wirkte plötzlich nachdenklich.

"Willst du nicht?", fragte Mirash.

"Ich bin noch nicht sicher.", sagte Flederschatten. "Ich glaube, es
steht mir nicht zu." Flederschatten entschränkte die Arme. "Jedenfalls
würde die Zeit-Fraktion nicht dieses riskante Setup erzeugen,
nur, damit ich die Dramaturge rette, und dabei trotzdem irgendwie
in Verdacht gerate, die Dramaturge zerstören zu wollen, was ohnehin
nicht so gut funktioniert. Und dann kommt noch oben drauf, dass sie
dich dazu auserwählt haben."

"Ich bin neu.", gab Mirash die Argumente wieder. "Ich habe den Mut dazu, ich
mag Chaos und gehe gern Risiken ein, und habe weniger zu
verlieren, als der Rest."

"Wirkt die Zeit-Fraktion auf dich so, als würde sie gerne
Risiken eingehen?", fragte Flederschatten herausfordernd.

Das war eine spannende Frage. Mirash geriet ins Grübeln, aber
Flederschatten unterbrach die Grübeleien im Keim.

"Ich manipuliere schon wieder ausversehen mit Fragen, die wirken, als
wäre eine bestimmte Antwort darauf richtig. Es tut mir
leid.", sagte fer.

"Es geht.", widersprach Mirash. "Ich merke es, dass du eine bestimmte
Antwort selber hast, aber es fühlt sich für mich an, als würdest
du offen sein dafür, dass ich dich von etwas anderem überzeuge."

Flederschatten nickte. "Das bin ich.", sagte fer und wechselte dann
kurz das Thema. "Hier unten
ist es kalt und ungemütlich. Magst du dich mit mir an den Tisch
zu einem Tee setzen? Oder ins Bett?"

"Das Bett klingt sehr gemütlich.", sagte Mirash und nickte.

---

Wenig später -- Mirash hatte die Virtualität noch einmal kurz zum
Teekochen verlassen -- saßen sie sich mit unsichtbaren Teetassen und
in Decken gehüllt im Bett gegenüber. Sie hatten den kleinen Beistelltisch
ins Bett gestellt, um darauf die Hände abzulegen. Mirash musste allerdings
aufpassen, die unsichtbare Tasse nicht darauf abzustellen, weil die
Keramik-Tasse sonst dadurch hindurchgefallen wäre. Sie spürte das
EM-Feld nicht. Sie konnte aber immerhin einfach durch das Bett hindurch
unters Bett gestellt werden. Oder kippelfrei in die Laken versenkt. Mirash
musste sie dann nur durch Ertasten wiederfinden, weil sie ja unsichtbar
war.

"Meinst du, die Zeit-Fraktion wollte mich loswerden?", fragte Mirash.

"Von allem, was ich mitbekommen habe -- und ich habe es nur am Rande
mal durch Flammenfinger oder in der Dramaturge aufgeschnappt --, wollten
sie dich zumindest nicht lehren. Und du nicht von ihnen gelehrt
werden.", sagte Flederschatten.

Da war es wieder. Dieses vertraute Gefühl, das Flederschatten in Mirash
auslöste. Als wäre fer einfach eine Person, die sich wirklich Gedanken
darum machte, wie es Mirash ging.

"Hast du dich von Anfang an für mich interessiert?", fragte Mirash. "Ich
hatte mich gefragt, ob du am Anfang viele Leute rettest und dich deshalb
mir nicht vorgestellt hast, weil, wenn du es immer tätest, du dich nicht
retten könntest vor Leuten, die dann an dir klebten."

Flederschatten schmunzelte. "Ich werde nicht von neuen Leuten
überlaufen.", sagte fer. "Ich weiß nicht genau, warum ich mich nicht
vorgestellt habe. Vielleicht, weil ich nicht gewohnt bin, dass mich
irgendjemand nicht kennt. Aber ich weiß es wirklich nicht mehr." Fer
machte eine kurze Pause, vielleicht um Gedanken zu sortieren, in der
fer aus fiener unsichtbaren Tasse nippte. "Als ich gefragt wurde, ob ich
dich zur Dramaturge bringen würde, war mein Interesse
an dir persönlich noch relativ gering. Das
passiert tatsächlich öfter, dass ich neue Leute erstmal in die
Dramaturge bringe. Oder manchmal Leute aus den Straßenkämpfen hole, die
nicht mehr dabei sein wollen. Sowas.", fuhr fer schließlich fort. "Aber
ich fand dich durchaus interessant, als du erzähltest, wie du
an Spiele herangehst, und dass du es auch mutig auf dieses anwenden
würdest. Ich mochte das sehr. Und dann hast du Zeit gewählt und, hui,
ja, das macht dich zu einer Person, hm, wie fasse ich das zusammen?"

"Die du gern ausspionieren würdest?", fragte Mirash und versuchte
fies zu klingen. Dann fiel ihm ein: "Vielleicht sollte ich diese
Frage nicht so stellen, wo ich das doch quasi mit dir vorhatte." Nun
hatte as es offiziell zugegeben.

Aber Flederschatten lächelte nur milde. "Das sind zwei verschiedene
Dinge. Wenn ich dich ausspioniere ist das was sehr anderes, als
wenn du es mit mir probierst.", sagte fer. "Und ja, ich hatte
ein schlechtes Gewissen, weil ich Dinge über dich in Erfahrung bringen
wollte, und habe dabei versucht, nicht zu krasse Sachen zu machen. Dass
ich mir Sorgen gemacht habe, rechtfertigt Ausspionieren in diesem
Spiel überhaupt nicht. Falls das beruhigt: Ich bin dir weder in die
Zeit-Basis nachgelaufen -- in die habe ich auch ohnehin keinen
Zutritt --, noch habe ich eine Person auf dich angesetzt. Ich weiß
von dir aber eben trotzdem ein paar Details, die in der Dramaturge
gefallen sind."

Mirash nickte zögerlich. "Warum ist es so sehr was anderes? Gehört
nicht einfach zu diesem Spiel, dass spioniert wird?"

"Vielleicht. Ein wenig." Flederschatten wiegte den Kopf nachdenklich
und sortierte dabei die Haare neu. "Aber du hast quasi deinen
Spielstand, und auch deine Möglichkeit, dich zu entwickeln, alles
komplett aufs Spiel gesetzt, nur um überhaupt in meine Basis zu kommen, und
hast nichts finden können. Ich dagegen habe die meiste Power im ganzen
Spiel, die auf eine einzelne Person kommt. Ich könnte dich, außer
in der Zeit-Basis, verhältnismäßig mühelos
stalken, ohne dass du es merkst."

Mirash nickte und grinste. "Wie eben. Also, Stalking war das nicht. Es
ist ja deine Basis, in der ich im Keller gelandet bin. Habe ich irgendeine
Falle ausgelöst, sodass du wusstest, dass ich da war?"

Flederschatten schüttelte den Kopf. "Der Bereich des Kellers tut nur
so, als wäre er geschützt.", sagte fer. "Da ist was
Schweres auf der Tür, aber das ist an ihr befestigt, das kommt nie
runter. Das Licht, falls du es gesehen hast, hat auch keine Funktion, es
tut nur so. All der Kram ist nur dazu da, dass Leute, nun ja, Zeit
brauchen, und denken, sie würden etwas hinkriegen."

"Wie fies.", murmelte Mirash. "Hat funktioniert."

Flederschatten grinste. "Ich habe nie gesagt, ich wäre nicht fies."

Mirash blickte ein paar Momente genießend auf dieses entsprechend fiese Grinsen, das
allmählich wieder verblasste. Selbst mit Nachthemd im Bett trugen sie
Masken. Das war auch irgendwie witzig. "Zurück zum Punkt.", sortierte
Mirash. "Ich verstehe nun, warum es was anderes ist, wenn ich dich
versuche, auszuspionieren, als umgekehrt. Ich verstehe noch nicht, welche
Sorgen du genau um mich hattest, und warum du so gelassen nimmst, was ich
dir bis jetzt versucht habe, anzutun. Oder ob du nun glaubst, dass
die Zeit-Fraktion mich loswerden wollte oder nicht. Wenn sie keine Risiken eingehen,
dann bin ich wohl kein großer Verlust für sie, sozusagen."

Flederschatten nickte. "Diesen letzten Schluss, den habe ich auch
ungefähr so gezogen.", sagte fer. "Mist, jetzt lege ich dir schon
wieder Schlüsse nahe. Die Zeit-Fraktion ist ziemlich
gut organisiert und strukturiert. Alle Leute haben einen Platz, alles hat
ein System. Soweit ist das grundsätzlich vielleicht nicht schlimm. Aber
dann kamst halt du zur Zeit-Fraktion, und sie konnten nicht so einfach
von dir profitieren. Du hast bei Flammenfinger lieber gelernt als in
der Zeit-Fraktion. Das ist der Schnipsel, den ich neben deinem Hang zu
Chaos mitbekommen habe."

Mirash nickte. "Mit Flammenfinger hat es mir gefallen.", sagte as. "Ich habe
mich schon gefragt, ob ich die Zeit-Leute damit sehr verärgere. Oder
ob ich was Schlimmes mache, wie Flammenfinger zu mehr Macht zu
verhelfen."

Flederschatten grinste abermals, es wirkte vieldeutig. "Und du hast
es trotzdem gemacht."

"Die Zeit-Fraktion wirkte nicht so ultra wütend." Mirash wirkte
nachdenklich. Auf die Sache war der Vorschlag gefolgt, mit dem
Mirash bei Flederschatten landen würde. "Sie haben mir nahegelegt, ich
könnte mich von dir ausbilden lassen. Dabei würdest du im Prinzip noch
mächtiger werden, aber sie meinten, bis es dir auf deinem Fähigkeitsstand
etwas bringen würde, hätte ich sehr viel in Erfahrung bringen
können und wäre wieder weg."

Flederschatten nickte. "Da haben sie auch recht.", sagte fer. "Und du
kämst mit Ausbildung und wahrscheinlich Information wieder. Oder
gar nicht. Aber wenn du nicht wiederkommst, hast du erst einmal fast
alle Spielenden gegen dich. Und das als neue Person im Spiel. Ich
vermute, die Zeit-Fraktion rechnet damit, wenn du nicht regelmäßig
an sie Informationen lieferst, dass das bisschen Spiel, was dir
übrig bleibt, für dich so langweilig oder unbefriedigend wird, dass
du einen neuen Account anlegst oder halt ganz aufhörst."

Mirash strich sich nachdenklich über das bartfreie Kinn. "Ich
habe durchaus von Anfang an einberechnet, dass ich hinterher vielleicht
nicht zur Zeit-Basis zurückkehren würde. Auch, dass ich vielleicht
einen Account-Neustart in Frage kommen lassen würde. Aber jetzt
widerstrebt mir letzteres aus Prinzip."

"Ich fände das auch schade.", sagte Flederschatten freundlich. "Aber
wichtig ist, dass du tust, womit du dich wohl fühlst."

Mirash trank vorsichtig einen weiteren Schluck des warmen Tees und
merkte, wie eine Anspannung von ihm abfiel. As überlegte noch, ob
as dies wirklich sagen wollte, aber entschied sich dann doch
dafür: "Ich fühle mich im Moment sehr wohl bei dir. Aber ich möchte
keine Abhängigkeitsbeziehungssache haben."

"Ich auch nicht." Flederschatten nickte und trank fienerseits einen
Schluck. Die Atmosphäre wurde dadurch behaglich, ruhig und gelassen. "Ich
habe mir in deiner Abwesenheit, aber vor allem bei deinem Spionage-Versuch
Gedanken gemacht, was ich dagegen tun könnte, dass es eines wird,
während ich dir gleichzeitig helfen könnte, wenn du Hilfe haben
magst. Magst du den Vorschlag anhören, oder möchtest du dir lieber
erst eigene Gedanken machen."

"Gleich.", schob Mirash auf. "Was ist überhaupt der Grund dafür, dass
du mir helfen möchtest? Ist es reine Selbstlosigkeit?"

"Puh.", machte Flederschatten. Fer sortierte ein weiteres
Mal das Haar hinter den Rücken, dieses Mal ausgiebiger. "Die Frage
nach der Selbstlosigkeit ist nicht so leicht zu beantworten. Das
weiß ich gerade nicht. Es gibt Varianten, über die ich sehr von
dir profitieren könnte, aber darum geht es mir nicht."

Mirash nickte. As hätte den Gesprächsstrang nicht unterbrochen, aber Flederschatten
hatte angefangen, sich die Haare zu flechten und das sah hinter
dem Rücken anstrengend aus. "Soll ich? Dir die Haare flechten
meine ich."

Flederschatten runzelte die Stirn und wirkte skeptisch.

"Es tut mir leid, das war wahrscheinlich eine übergriffige
Frage.", fiel Mirash ein. "Wir haben so einen seltsamen
Zustand von Nähe und Distanz."

"Stimmt auch wieder.", sagte Flederschatten und ließ die Hände
sinken. "Es muss fest werden. Tu mir dabei ruhig weh."

Dieses Mal war Mirash es, das die Stirn runzelte. Aber as nickte
trotzdem. "Du gibst mir bestimmt Anweisungen, wenn ich dir mehr oder
weniger weh tun soll."

Flederschatten beförderte den Tisch aus dem Bett und drehte
Mirash den Rücken zu. Die Kröte hopste auf fienen Schoß. "Jedenfalls
lässt sich die Frage vielleicht
mit einer Kurzzusammenfassung beantworten: Du kamst ins Spiel, ohne
Hintergründe zu kennen, und dann kam raus, dass du zur Zeit-Fraktion
geraten bist. Von dem winzigen bisschen schließend, das ich dich kennen
gelernt habe, wärest du dort sehr wahrscheinlich nicht
gelandet, wenn du dich vorher ausgiebig informiert hättest. Damit
bist du also eine Person, die ich eventuell mag, deren Motive
ich nicht beschissen finde, in einem sozialen Gefüge, das ich
gern zerlegen möchte. Mist."

Flederschatten wirkte nicht, als hätte fer zu Ende geredet. Ein
Schauer lief durch fienen Körper, als Mirash das Haar in drei
Stränge aufgeteilt hatte und saine Hände sich im Nacken
sortierten. Es fühlte sich wunderschön zwischen den Fingern
an. Mirash fragte sich, ob Flederschatten im realen Leben auch
solches Haar hatte. Und dann fiel ihm ein, dass Mirash hier wahrscheinlich nur
das virtuelle Haar flocht, das durchaus am Kopf ziehen konnte, aber
nur Flederschatten oder eine Person im gleichen Spielraum könnte
Flederschattens reales Haar flechten, weil es nichts magnetisches
an sich hatte. "Ähm,", machte Mirash
deswegen. "Wie ist das mit den Haaren eigentlich für dich? Hättest
du selbst auch dein reales Haar geflochten?"

Flederschatten kicherte leise. "Ich habe mich schon gefragt, wann
du mit deiner Expertiese zu EM-Anzügen was ansprichst.", sagte
fer. "Kennst du EM-Spray für Haare?"

"Wow, ja, aus Berichten und Artikeln. Ich habe noch nie eine
Person erlebt, die sowas auch benutzt." Na dann, dachte Mirash,
legte die ersten zwei Windungen für das Flechten und zog so
stramm, dass der Körper vor ihm zuckte und Flederschatten ein
leises Geräusch von sich gab. "Gut so?"

"Ja.", antwortete Flederschatten. Nicken konnte fer nicht. "Die
Haare sind real leider nicht ganz so lang. Aber die Virtualität
übersetzt das gut, sodass sie sich so anfühlen, als wären sie
es. Ich mag Haargefühl."

"Ich auch." Mirash zog auch die nächsten Windungen so
fest, dass Flederschatten zuckte und den Rücken durchspannte. "Wenn
sie so stramm geflochten sind, kann ich leider nicht so gut mit den
Fingern von unten hineinfahren und meine Hand darin verkrallen." Die
nächste Windung kam nicht mehr so sehr auf der Kopfhaut an und
Flederschatten hätte vielleicht entspannen können, wären da
nicht Mirashs Worte gewesen. Mirash entging das kurze, rasche
Einatmen nicht. "Weiter im Text. Warum hast du 'Mist' gesagt?"

"Ich sollte dir gegenüber vorsichtig sein mit Planäußerungen dazu, dass
ich die Zeit-Fraktion zerlegen möchte, solange ich mir nicht
halbwegs sicher bin, was du mit den Informationen machst.", sagte
Flederschatten. "Ich stehe in dem Konflikt, dass ich durchaus
gern sehr transparent mit dir sein möchte, aber du mir
auch Pläne durchkreuzen kannst."

"Es ergibt sich daraus für mich natürlich die Frage, warum du die Zeit-Fraktion
zerstören möchtest.", hielt Mirash fest. "Und ist es dann sowas
wie ein Wunsch zu beschützen, weshalb du mir helfen möchtest?"

"Beschützen passt einigermaßen, aber eigentlich möchte ich
dich vor allem nicht unwissend zwischen Fronten
geraten lassen, von denen ich eine selbst verkörpere.", fasste
Flederschatten zusammen. "Ich möchte dich da nicht hineingeraten
lassen, ohne dass du weißt, in was du da steckst, und selbst
aussuchen kannst, wie du darin stecken möchtest. Also
ist es vielleicht Gerechtigkeitsempfinden?"

"Klingt soweit nachvollziehbar.", überlegte Mirash. "Und
wie schnell lässt sich beantworten, warum ihr euch gegenseitig
zerlegen wollt? Und wie funktioniert zerlegen in einem Spiel
mit Respawn?"

Flederschatten gab ein schmunzelndes Geräusch von sich. "Du
stellst nun schon wieder diese Frage und dann
noch eine einfachere hinterer.", sagte
fer. "Beim Sterben verlieren wir Level und etwas wird von den Fähigkeiten
angeknappst. Flammenfinger hat zum Beispiel nun die frisch
erworbene Zeit-Magie wieder eingebüßt, weil ich sem umgebracht habe."

"Wow." Das war Mirash nicht bewusst gewesen. "Warum wollte
Flammenfinger sterben?"

"Flammenfinger hat indirekt einen Aufruf eingeleitet, mich zu töten, weil
ich angedroht habe, alle Anwesenden zu töten." Flederschatten drehte sich
wieder um, um Mirash mit gerunzelter Stirn anzusehen. Mirash war
ausreichend tief mit dem Geflecht angekommen, dass das
möglich war. "Flammenfinger steht im Zweifel immer für die Dramaturge
und für die Anwesenden, die keine Kampfansage machen. Obwohl prinzipiell
in diesem Zusammenhang alle eine gemacht haben. Und, nunja, es wäre vielleicht
schon auffällig gewesen, wenn Flammenfinger meinen Angriff überlebt hätte."

"Legt ihr mir einfach so vollkommen transparent offen, dass ihr euch
gut kennt und miteinander arbeitet, während das kein so allgemein
bekanntes Wissen ist?", fragte Mirash. "Weiht ihr mich da in etwas
ein, also, speziell mich?"

Flederschatten nickte. "Flammenfinger war dafür, ich war dafür.", sagte
fer leise. Fer nahm Mirash das Zopfende ab und band es mit einem Zopfband
zusammen, das auf der Bettkante lag. Dann nahm fer die im Bett versunkene,
unsichtbare Tasse wieder auf, etwas danach tastend, und trank einen Schluck.
"Und nun zu der Frage, warum ich die Zeit-Fraktion zerlegen möchte.", murmelte
fer. "Und mit Zerlegen meine ich, solange töten und wieder töten, bis sie annähernd
alle unter Level 5 kommen und nicht mehr ausbilden können."

Mirash hob eine Braue und runzelte dabei die Stirn. "Das ist schon
ein krasses Anliegen."

"Ich nehme an, sie wissen davon nichts?", fragte Flederschatten.

Mirash zögerte, bevor as nickend bestätigte. "Meines
Wissens nicht, muss ich dazu sagen.", sagte as, und überlegte: "Das
hätte zur Folge, dass du die einzige Person der
Zeit-Fraktion werden würdest, die Fähigkeiten skillen kann, indem
du lehrst. Du kannst dir dann aussuchen, wen du lehrst, also wer
noch irgendwann wieder skillen kann. Weil ja nur beim Lernen oder beim
Lehren gelevelt wird und nur beim Lehren geskillt. Und
du bist dann die einzige Person, die lehren kann."

Flederschatten nickte. "Genau. Du bist schnell mit schließen!" Fer
fügte in die unsichtbare Tasse schmunzelnd hinzu: "Was mich nicht überrascht."

"Wobei, das stimmt nicht.", korrigierte sich Mirash. "Es können
auch Leute lehren, die nicht zur Zeit-Fraktion gehören."

Flederschatten nickte wieder. "Korrekt."

"Also, nun endgültig die Frage: Warum?", fragte Mirash.

"Einige Gründe. Fangen wir an mit meinem Lieblingsgrund: Chaos!"
Flederschatten grinste vorsichtig, ein wenig unsicher vielleicht. "Es
ist nicht der stärkste Grund, aber ich mag, dass darauffolgend etwas
passieren muss, was alte Strukturen bricht, und nicht ganz klar
ist, wie. Entweder folgt, dass effektiv
nur noch vier Fraktionen mit einander kämpfen. Oder die Spielentwickelnden
müssen das Update kippen, dass nur Spielende ab Level 5 lehren dürfen. Das
war noch nicht immer so. Das ist hinzugekommen, damit sich neue Leute an
Personen wenden müssen, die schon ein wenig dabei sind, um Durchmischen
der Generationen zu motivieren." Flederschatten zuppelte nun doch ein paar Strähnen im
Zopf zurecht, beschwerte sich aber nicht. "Oder aber ich und Personen
aus den anderen Fraktionen entscheiden, wie wir Zuwachs der Zeit-Fraktion
fördern. In jedem Falle ist meine Hoffnung, dass eine neue, weniger
toxische, weniger gatekeepende, weniger elitäristische Struktur in der Zeit-Fraktion
wachsen kann." Flederschatten machte eine bewusste Sprechpause
zum Atmen und fuhr in sanfterem Ton fort: "Und nun habe ich all die bösen Worte in den Mund
genommen. Toxisch, Gatekeepend, Elitär. Die Beschreibungen würdest du
alle kennen, wenn du Foren gelesen hättest. Vielleicht hast du auch
ein bisschen davon schon hier im Spiel wahrgenommen. Aber vielleicht auch nicht so
sehr. Ich würde dich da gern selbst beurteilen lassen. Diese
Strukturen jedenfalls sind meine Motive, die Fraktion zu zerlegen, weil ich glaube, dass
das ganze soziale Gefüge in Lunascerade, unter allen
Fraktionen zusammen, davon profitieren könnte und alle mehr Spaß
haben könnten, wenn die Zeit-Fraktion weniger toxisch wäre. Die
Einstellung wirst du tatsächlich viel finden. Die Meinung ist verbreitet, aber
es gibt sehr verschiedene Ansichten, wie damit umgegangen werden
sollte. Nun kennst du meine."
