Namen
=====

**CN: Reden über Deadnames, aber sie fallen nicht. Nacktheit. Nachdenken
über Berührungen im Genitalbereich**

*Mirash*

Es hätte ihm klar sein sollen. Aber es hatte drei Wochen gebraucht, bis
as die Buchstaben des Namens *Mirash* in das Namensfeld für
die Accounterstellung eingegeben hatte. In der Zwischenzeit
hatte as alle anderen Daten in der Maske
bestimmt fünf Mal perfektioniert. Den
Skin -- das Aussehen des Charakters, den as spielen wollte -- vor allem. As
hatte sich auch ein paar Mal bei der Schreibweise der
eigenen Pronomen hin und her umentschieden, aber in dem Beispielsatz
gefiel ihm 'as/sain/ihm/as' am besten. Der Beispielsatz war überraschend
kurz, um alle grammatikalischen Fälle der Fürwörter
abzudecken: "Als sich die Entität as näherte, nahm as deren Hände
in saine und es ging ihm besser.". Die Schreibweise
wäre auch sehr schnell erklärbar: Wie 'es/sein/ihm/es', nur, dass jedes 'e' durch
ein 'a' ersetzt wurde.

Eigentlich war es ja jedes Mal so. Ein neues Gerät, ein Faltrechner
oder Taschenrechner etwa, wurde aufgesetzt und
brauchte einen Namen? Es betraf schließlich nicht nur as, dann die
weitere Einrichtung aufzuschieben, über Stunden, manchmal über Tage, bis
der richtige Name für das Gerät gefunden war. Und so war es eben
auch mit Account- oder Profilerstellung.

Aber in diesem Fall wäre es einfach gewesen. Der Name Mirash hatte
schon viel länger in sainem Kopf herumgespukt. Weil er ihm gefiel. Weil
as überlegte, sich auch im Outernet so zu nennen. Und dieses Spiel, in dem as gedachte,
längerfristig zu verweilen, fühlte sich nun wie ein guter Raum an, den
Namen auszuprobieren. Ähnlich wie andere Leute Geschichten
mit neuen Namen oder Pronomen schrieben, um sich in diese im Vorfeld
hineinzufühlen, bevor sie sie ihrem sozialen Umfeld als ihren
neuen Namen oder ihr neues Pronomen mitteilten, sollten sie sich
im anderen Universum als passend herausstellen.

*Lunascerade* hieß das Spiel. Und as hieß nun *Mirash*.

Der Name hallte durch Mirashs Gedankenuniversum und malte alles ein
bisschen anders an. Es wirkte darin nun weiter, wohnlicher, die
Luft war frischer, die Atmosphäre angenehm kühl und schattig. Aber
auch noch etwas ungewohnt, wie nach einem
Umzug, in eine neue Wohnung, die definitiv besser war als die
alte, aber in der noch nicht alles eingerichtet war, und die weniger
tollen Dinge noch nicht entdeckt.

As lächelte in sich hinein aber wartete trotzdem noch damit, zu
bestätigen und den Account auch anzulegen. Einfach nur für einen bewussten
Spaziergang in die Küche, um sich einen Tee zu machen, noch
einen halben Rundling zu essen und sich in diese Freude über
die Entscheidung hineinzufühlen. Und in die Unsicherheit. Mirash
gehörte zu den Leuten, die Unsicherheit in gewissen Dimensionen
durchaus genoss.

As spazierte zurück ins Spielzimmer und stellte den dampfenden
Becher auf das dafür vorgesehene Regalbrett. Dann war umziehen
dran. As entledigte sich all sainer Kleidung und zog stattdessen
den EM-Anzug an. Ein Anzug, dünn und weich wie eine zweite
Haut, die passgenau anlag. As tat es bewusst, fädelte die Zehen
sorgfältig in die dafür angefertigten Anzugzehen, rollte ihn
die Beine hinauf, sortierte ihn besonders im Schritt, damit er
zugleich nicht irgendwie klemmte und trotzdem alle Stellen des
Körpers berührte. As glaubte eigentlich nicht gleich am Anfang
in der Virtualität eine Person zu treffen, mit der sich as so annähern
würde, dass sie sich im Schritt anfassten, aber manchmal passierten
überraschende Dinge. As war da recht offen. Dann über den Torso, um
die flachen Brüste herum, wo der Anzug für sainen Geschmack noch
viel zu locker saß. Aber das würde das elektromagnetische Feld, kurz
EM-Feld, gleich regeln. Der Raum konnte ein solches erzeugen und
der Anzug das Gegenfeld, sodass eine im Anzug anfassbare Wirklichkeit im
Zimmer erstellt werden konnte. Eben auch ein sehr guter, passgenauer
BH.

As fädelte auch die Arme in den Anzug, zupfte die Finger besonders
gründlich zurecht, bevor as sainen Kopf in Kapuze und Gesichtsteil
verpackte. Der letzte Schritt war, die sehr dünne Anzughaut über die
Lippen zu haften. Für viele war das etwas Gewöhnungsbedürftiges. As
hingegen hatte es von Anfang an geliebt. Die feinen Berührungen auf den
Lippen von etwas, was sachte darauf haftete, aber gefühlt nicht klebte. Etwas,
was sich nur fast perfekt mitbewegte, wenn as die Lippen schürzte. As
lächelte. Das hingegen konnte diese Anzughaut ohne Probleme mitmachen.

As hatte einen Anzug, bei dem die VR-Brille direkt integriert war, und
schaltete die Virtualität ein. Die Eingabemaske schwebte vor ihm. Die
ausgewählte Kleidung für den Spielstart wuchs schichtweise an
sainem Körper. Sie fühlte sich gut an. Sie war eng, der Stoff fest und
sicher einen halben Zentimeter dick, aber trotzdem geschmeidig. Sie
hatte, wie für Anfangende üblich, keinen besonders hohen Rüstwert, war
ihm beim Aussuchen erklärt worden. In Farben hatte as fast freie
Auswahl gehabt, alles außer Schwarz. Was as irritiert hatte: Eigentlich
hatte as sich das Spiel wegen des Ambientes und der Atmosphäre
ausgesucht. Alle Einblicke, die as in Lunascerade erhascht hatte, hatten
ein düsteres Spiel gezeigt. Viel nasses Kopfsteinpflaster, Brücken,
Regen in der Dämmerung oder Nacht, Fackelbeleuchtung. Personen, die
darin spielten, waren überwiegend schwarz mit bunten Farbeinsätzen
gekleidet gewesen. Körperbetonte Anzüge oder in Kleidern von zierlich bis
wuchtig, oft mit Spitze. Ein Stil, mit dem sich Mirash mehr als
wohl fühlte. Weshalb Mirash ohne viel Vorbereitung beschlossen
hatte, dieses Spiel mit Learning-By-Doing-Methode kennenzulernen. As hatte keine
Ahnung, ob es sich um ein Spiel handelte, für das diese Lernmethode
sonderlich geeignet wäre, aber as stürzte sich durchaus gern
unvorbereitet ins Chaos und mochte überrascht und überfordert werden.

Mirash hatte sich überlegt, wenn nicht schwarz, dann rosa. As trug also
nun einen hautengen rosa Anzug aus einem leder-ähnlichen Material und
einen rosa Zylinder.

Mirash wärmte sich vorsichtshalber mit ein paar Dehnübungen auf, und
legte dann endlich den Account an. Mit dem Anlegen landete as auch im Spiel.

---

Dieses Spiel verzichtete auf einen Prolog oder ein Einleiten der Geschichte. Mirash
stand unvermittelt dicht an einer Häuserfassade auf einem Gehweg unter einer Brücke, oder
fast mehr in einem kurzen Tunnel. Der Schein
einer entfernten Fackel und Mondlicht erhellten den knöcheltiefen Bodennebel, der in
dünnen Schwaden über das feuchte Pflaster waberte. Von etwas weiter oben drangen
Geräusche herab, vielleicht von der Brücke, vielleicht von den Häusern in der Gasse. Stimmen,
Sirren, Klackern, aber nicht so laut, dass Mirash es als Lärm bezeichnet hätte. As
sog die Atmosphäre in sich ein wie kühle Luft vermengt mit Freude und einer Prise Grusel.

Mirash hatte gerade überlegt, unter der Brücke hervorzuschauen, um sich nach den Ursachen
der Geräusche umzuschauen und sich zu orientieren, da regnete
eine Person über das Geländer der Brücke und
schlug mit dem Rücken auf das Pflaster nur drei Schritte neben ihm auf. Die Person
hielt sich nicht lange mit Herumliegen oder Leiden auf, was in
Virtualitäten nicht unüblich war, sondern sprang beinahe flink wieder auf die
Beine, einen Bogen bei der ganzen Prozedur nicht loslassend. Sie legte nun einen
Pfeil auf die Sehne und zielte irgendwo nach oben. Dabei
fiel ihr Blick auf Mirash. Nur ein kurzer Moment der Ablenkung, ausreichend, dass ein anderes
Geschoss, das Mirash so schnell mit dem Blick nicht erfassen konnte, auf die
Person zuraste und die Person einmal von oben bis unten durchdrang.

"Shit!", fluchte die Person, ihr Erscheinungsbild flackerte kurz und tauchte eine Körperbreite
neben sich wieder auf.

"Ich bin auf eine Illusion reingefallen?", rief eine weitere Person energiegeladen von oben
herab, vielleicht die, die geschossen hatte. Aber statt ein weiteres Mal direkt
zu schießen, sprang sie ebenfalls von der Brücke und landete neben der ersten.

Die erste floh zu Mirash unter die Brücke. Dabei fiel Mirash auf, dass beide
tatsächlich überwiegend schwarze Kleidung trugen. Die erste Person mit dem
Bogen trug einen violetten Umhang dazu, den Anzug der anderen zierten
orangene Einsätze. Zugehörigkeitsfarben?

Mirash kam nicht viel dazu, etwas zu schließen. Unter der Brücke verknäulten
sich die beiden vor sainen Füßen in einen Kampf mit Messern, sodass es eng
für Mirash wurde. As versuchte, den Schutz der Brücke erneut zu verlassen, aber
weitere Kämpfende regneten vom Himmel und das Kampfgetümmel wurde
unübersichtlich. In gewisser Weise auch schön, aber unübersichtlich. Der
Bodennebel sammelte sich anderswo. Gelegentlich verschwanden Leute
und ließen aufgewirbelte Löcher im weißen Gewaber zurück. Sie despawnten, mutmaßte
Mirash, um irgendwo anders wieder aufzutauchen, oder
starben? As fragte sich, ob as sich wenigstens darüber hätte zuvor informieren
sollen. Es wäre schon interessant zu wissen, ob as im Fall von Sterben gleich
den nächsten Account anlegen müsste, oder irgendwo respawnen würde.

Irgendeine Person krachte direkt gegen Mirash und mit ihm gegen die Tunnelwand, gerade
als Mirash dachte, eine Lücke zum Entwischen gesehen zu haben. "Es tut mir
leid!", sagte sie. "Ich kann schwer genug auf mich selbst aufpassen. Versuch wegzukommen!"

Irgendwodran war sehr erkennbar, wagte Mirash zu schließen, dass as nicht zu den
Kämpfenden gehörte. Vielleicht kannten sie sich alle. Vielleicht lag es daran, dass
Mirash kein Schwarz trug.

"Ich erkämpfe dir eine kurze Lücke, und dann lauf, ja?", keuchte die Person, als
sie sich von Mirashs Körper wieder weggedrückt hatte.

Mirash rückte den Zylinder zurecht und nickte. Die Person machte
eine Werfbewegung schräg zu Boden, weg von dem Ausgang unter der
Brücke, an dem Mirash stand. Die Luft flackerte und funkte orange in
Wurfrichtung, und davon ausgelöst bewegte sich eine
Welle durch den Boden, als wäre er flüssig. Eine Welle
mit der praktischen Eigenschaft, nicht so durchdringbar wie Wasser
zu sein, sodass sie die meisten von den Füßen riss. Sie rappelten sich eine halbe
Schrittweite weiter wieder auf und Mirash hatte tatsächlich
Gelegenheit, aus der Enge zu fliehen.

Allerdings schien die Unterführung nur der Knotenpunkt des Kampfs geworden zu sein. Überall
rangelten nun Paare, Triaden oder auch mal vier Personen miteinander. Geschosse
flogen über die Straße, ein paar davon auch wieder mit Funken. Mirash
entdeckte im eigenen Sichtfeld eine Lebensbar, die schon ein
bisschen gelitten hatte. As begann zu rennen, um irgendwo Deckung zu
finden. Einen Pfeil im Fuß später schrumpfte die Bar noch einmal
auf die Hälfte. Der Pfeil störte beim Rennen, also bückte as sich, um ihn
zu ziehen. Dabei stellte Mirash fest, dass er nicht nur durch
seine das Laufen behindernde Platzierung in sainem Körper gestört hatte, sondern
auch durch eine Kraft ohne sichtbaren Ursprungs, die am Pfeil gefühlt umso
doller zerrte, als as ihn nur noch in der Hand hielt. As hätte ihn vielleicht festhalten
können, war aber kurz zu überrascht gewesen und hatte ihn
losgelassen. Er war nebelaufwirbelnd davongedüst. Auch gut. Im Gegensatz
zum nun pfeilfreien Körper war die gefährlich
verringerte Lebensenergie wohl nicht so unbedingt von Vorteil.

Zwei Straßenecken weiter entdeckte Mirash eine wirklich schmale Gasse, in
die as sich gerade so quetschen konnte. Sie wirkte immerhin
zumindest im Moment sicher. Irgendwo musste
es brauchbar sichere Orte für Anfangende geben. Oder bessere Zeiten. Vielleicht
war es einfach Kampf-Tag oder so etwas.

Mirash hatte keine große Lust, das Spiel zu verlassen und erst später wieder
anzufangen. As hatte sich doch gerade erst darauf eingelassen. As hatte sich
auf diese ganze Unkalkulierbarkeit eingelassen und wollte sie jetzt auch haben. Gucken, was
passierte. Es waren überall Häuser und Brücken. Mehrere Stockwerke an
Brücken. Vielleicht sollte as versuchen, in ein Haus
hineinzukommen oder eines mit einer Bestimmung zu finden.

As blickte aus der Nische heraus, nur um zu schauen, ob vielleicht irgendwo ein
Langkommen wäre, als ein gleißend helles Licht as unvermittelt blendete. As
verzog sich, die gleiche Bewegung von eben rückwärts wieder ausführend,
in die schmale Gasse und sah nichts. As blinzelte mehrfach, wartete geduldig
ab, bis wieder Schemen ausmachbar waren. Überleben war dann wohl gerade
mehr Zufall. Geblendet und im Dunkeln war es immerhin praktisch, rosa
Kleidung zu tragen. Diese hob sich wenigstens ab.

"Moin!", grüßte eine Person.

Mirash hatte sie -- kaum verwunderlich -- nicht kommen sehen. Und sah
auch jetzt, als as sich zur Stimme umwandte,  nicht viel von
ihr. Sie war sehr dunkel gekleidet, bis auf einen kleinen orangenen
Fleck an der Kapuze oder dem Umhang im Nacken. "Moin.", grüßte Mirash
zurück. Das war eine klassische Begrüßungsfloskel im Nord-Westen Maerdhas, wo
Mirash gar nicht wohnte, aber as kannte sie trotzdem und mochte sie gern.

"Du wirkst hier ein bisschen verloren.", sagte die Person freundlich. "Kann
ich helfen?"

"Ich bin vor allem neu.", antwortete Mirash. So gut war as nun nicht
mit der ganzen Vorstellungssache.

"Neu wie in, erst ein paar Tage hier, oder neu wie in, gerade das erste
Mal eingeloggt?", fragte die Person.

"Letzteres.", gab Mirash zu.

Die Person gluckste. "Hast du dich absichtlich zur Trainings-Kombat-Zeit
eingeloggt, oder wusstest du davon nicht?"

"Auch letzteres.", antwortete Mirash. "Ich habe mich vorher nicht
über das Spiel informiert. Ich habe lediglich Einblick in die Atmosphäre
aus Video-Zusammenschnitten bekommen. Ich gehöre zu diesen extremen
Leuten, die sich Spiele gern unvorbereitet antun."

"Hui!", machte die Person neben ihm. Es wirkte nicht abwertend auf
Mirash.

Es war nicht das erste Mal, dass
as diese Strategie vorstellte. Die Reaktionen darauf waren
unterschiedlich. Teils abhängig davon, wie gut sich das jeweilige Spiel
eben dafür eignete, von Unverständnis, teils Skepsis oder leichter
Abwertung über Neugierde oder Nachdenklichkeit bis hin zu Begeisterung
oder Bewunderung. Bewunderung vor allem dafür, weil sich viele nicht
so recht vorstellen konnten, dass Mirash fast überhaupt kein Problem
damit hatte, zu scheitern.

Die Person schien in die Kategorie Nachdenklichkeit zu fallen. So
nahm Mirash das zumindest wahr.

"Gilt dein Konzept nur für Vorbereitungen außerhalb der Welt, oder
ist es etwas anderes für dich, wenn du in der Welt von Spielenden
aufgeklärt wirst?" Die Person sortierte einen langen, dunklen, geflochtenen
Zopf nach vorn. "Vielleicht sogar in einer Weise, die Spielmechanik
sein könnte, wobei, ich bin nicht sicher."

"Letzteres klingt sehr interessant." Mirash blinzelte noch ein paar
Mal, in der Hoffnung, bald wieder klarer sehen zu können. "Bei
ersterem kommt es ein bisschen drauf an. Es könnte mir ja in
einem denkbaren Extrem eine Person In-Game Foren-Texte vorlesen."

"Das verstehe ich. Das wäre auch nicht sehr, wie heißt das? Immersiv?
Die Immersion würde nicht funktionieren?" In der Stimme konnte
Mirash ein Lächeln oder zumindest eine Freundlichkeit ausmachen.

"Ich hab's nicht so mit Fachwörtern. Also, mit vielen nicht.", gab
Mirash zu.

"Immersion bedeutet ungefähr, dass das Spiel oder eine Geschichte oder
so für dich gerade zu der Welt wird, in der du gefühlt lebst, und
nicht eine, die du eher von außen wahrnimmst.", erklärte die Person.

"Wenn wir in einem Spiel über das Spiel reden, ist dann Immersion
zum Scheitern verurteilt?", fragte Mirash.

Die Person kicherte. "Für mich nicht. Für dich? Fühlst du
dich durch unser Gespräch weniger als Teil dieser Welt? Gibt
es dadurch für dich Entfremdungseffekte?"

Mirash konnte zunehmend
die Körperhaltung ausmachen. Die Person lehnte relativ lässig
an der Häuserkante direkt neben dem Gassenzugang. In
Mirashs Erinnerung war dahinter nicht allzu weit entfernt
Kampfgetümmel gewesen.

Mirash rief sich die Frage mit der Immersionsstörung zurück
in Erinnerung. "Tatsächlich nein. Mich reißt eher
raus, dass du da so ruhig lehnen kannst. Während
der, wie hast du sie genannt, Trainings-Kombat-Zeit."

Die Person blickte sich um. Gründlich in beide Richtungen. "Sieht
relativ leer aus im Moment. Der Kampf findet in ganz Fork statt, so
heißt die Stadt, und verschiebt sich darüber wie Gewitterwolken." Die
Person zögerte und fügte dann hinzu: "Wobei ich schon einen
gewissen Einfluss hatte."

"Fork. Wie die Hauptstadt Maerdhas, also außerhalb der
Virtualität." Mirash kicherte. Die Hauptstadt des Kontinents
Maerdhas hatte tatsächlich auch mehrere Stockwerke an Brücken
und Häusern. "Warum hast du den Einfluss genommen?"

"Immersion oder Ehrlichkeit?", fragte die Person.

Mirash runzelte die Stirn. Was die Person vielleicht nicht sehen
konnte. Es sei denn, sie hatte Nachtsicht oder so etwas. Und
konnte durch Masken gucken, fiel Mirash ein. Das Spiel hieß auch Lunascerade, abgeleitet
von Mascerade, weil zur jedem wählbaren Kleidungsset auch eine Maske gehörte, die relativ
frei gestaltet werden konnte, aber meistens glitzerte oder mit
Spitze besetzt war, und das halbe Gesicht verdeckte. "Ginge
Ehrlichkeit schnell?"

Die Person nickte.

"Dann vielleicht doch Ehrlichkeit.", entschied Mirash zögerlich.

"Eine der Personen, die dir vorhin im Kampfgetümmel unter der
Brücke begegnet ist, hat mich auf einem anderen Kanal angeschrieben, dass
du vielleicht Hilfe gebrauchen könntest." Eine gewisse Unsicherheit
lag in der Stimme der Person. "Ich würde dir Geleitschutz zur
Dramaturge anbieten. Die Dramaturge ist ein kampffreier Raum, eine
Art Kulturtreff."

"So etwas habe ich gesucht.", fiel Mirash wieder ein. Ein Haus,
oder so, mit Bedeutung. "Ich würde das Angebot annehmen. Ist
für dich dafür wichtig zu wissen, dass meine Lebens-Bar irgendwo
im roten Bereich ist."

Die Person atmete in einer Weise, die belustigt wirkte. "Ich
habe halb damit gerechnet.", sagte sie. "Ich bekomme dich
so versehrt, wie du gerade bist, in die Dramaturge. Dort gibt
dir bestimmt eine Person ein bisschen Heilung aus."

Jetzt, als der Plan geklärt schien, hatte die Person es plötzlich
eilig. Sie brach einfach auf und Mirash folgte. Sie ging in
diesem Tempo, das Leute voranlegen, die merken, dass die Begleitperson
gerade so mithalten würde, immer einen halben Schritt hinterher. Etwas, was
Mirash im Normalfall aufgeregt hätte, aber gerade hatte es vielleicht
einen anderen Zweck, als dass einfach die schnellere Person
aus eigener Bequemlichkeit ein höheres Tempo einforderte. Auf ihrem
Weg blickte sain Geleitschutz konzentriert in alle Richtungen, führte
as gefühlt im Zickzack durch das Straßennetz, Rampen hinunter, über
Brücken und unter anderen entlang. Mirash sah entfernt Kampfgetümmel
und vermutete, dass die Person as bewusst mit Abstand an diesem
vorbeiführte. Nur einmal wurde es eng. "Lass mich fünf Schritte
vorgehen.", bat die Person, als sie durch eine Straße mussten, in
deren Mitte drei Personen Waffen auf einander richteten. Mirashs
Begleitung näherte sich den Kämpfenden wie eine Person, die sich einer
Gesprächsgruppe näherte, die noch nicht wusste, ob sie willkommen
wäre oder eine Lücke in einem Gespräch abwartete. Als sie die Aufmerksamkeit
bekam, kommunizierten sie kurz, Mirash verstand sie nicht, aber die
Kämpfenden verzogen sich darauffolgend woandershin.

"Das ging unkomplizierter, als erwartet.", sagte die Person, als Mirash
aufschloss. "Wir sind fast da."

"Kennst du alle?", fragte Mirash.

Die Person lachte, hielt sich dann aber auf. "Entschuldigung. Der
Gedanke war absurd, ich wollte dich nicht auslachen. Es spielen hier
so um die tausend Leute mit. Es gibt einiges an Fluktuation. Und
mein Namensgedächtnis ist gar nicht Mal so gut."

"Ich heiße Mirash." Warum sagte as das nun? "Ich glaube, das
war die denkbar unempathischste Reaktion."

Die Person ging nicht darauf ein. Sie blieb auf einer
Straße stehen, die Mirash zunächst
für eine Brücke gehalten hatte, weil zumindest auf der einen
Seite davon keine Häuser waren. Nun blickte as genauer hin und
erkannte, dass hinter der kleinen Begrenzungsmauer Wasser
schwappte. Auf der anderen Seite ragte ein einzeln stehendes
Haus aus dem Wasser. Sie standen direkt vor den großen Holztüren. Ein
durch Alter gezeichnetes und dadurch wunderschönes Schild
rostete darüber mit der Aufschrift "Dramaturge" vor sich hin.

Mirashs Blick wanderte zurück zum Gesicht der Person. As konnte
das erste Mal ein Lächeln darin nicht nur hören, sondern auch
ausmachen, aber noch waren alle Umrisse verschwommen und blass. Die
Dramaturge war außen mit Fackeln beleuchtet, das half
ein wenig. Die Person
verbeugte sich kurz sachte, wodurch das Orange an der Kapuze
noch einmal in Mirashs Sichtfeld kam. Oder war es doch etwas
anderes, als eine Kapuze? Ein Schal in der Kapuze? Die
Person hielt Mirash die Tür auf, worauf as beschloss, der
Frage später nachzugehen und erst einmal einzutreten. Aber
anders, als Mirash bei der Geste vermutet hätte, folgte die
Person nicht mit hinein.

Sie hatte sich nicht vorgestellt. Von der ersten Person, mit der
Mirash hier also interagiert hatte, -- sah as von dem sehr kurzen
Gespräch mit der Person mit der Energiewelle ab, kannte as also keinen
Namen. As schmunzelte. Es war kurz ein Gefühl von Enttäuschung oder
Leere gewesen, als die Person hinter ihm verschwunden war. Aber
es war auch das gute Recht der Person, sich nicht weiter um
as zu kümmern.

Und bezüglich des Namens: Es konnte so viele Gründe geben, den eigenen
Namen nicht zu nennen. Naheliegend wäre vielleicht, dass es
für die Person eine verminderte Rolle spielte, weil ihr
Namensgedächtnis nicht gut war. Oder dass sie einfach sehr
viele Leute traf, oder auch nicht so gut in Gesprächsführung
war, oder abgelenkt gewesen war, sodass sie nicht daran gedacht
hatte. Oder dass sie
in diesem Spiel sowas wie ein Villan war und deshalb anonym bleiben
wollte, könnte auch sein. Oder
dass sie von Anfangenden überlaufen würde, wenn sie jeder neuen Person
sagen würde, wer sie war. Oder es gab in diesem Spiel eine
Mechanik, dass den eigenen Namen zu sagen, nachteilhaft
wäre. So etwas Albernes wie Macht über Leute, deren Name
bekannt war. Das wäre jetzt schlecht, denn as hatte sich ja
vorgestellt. As hielt sich nicht mit Gruseln auf.

Und der letzte mögliche, wenn auch vielleicht unwahrscheinliche
Grund, der Mirash einfiel, war, dass der Person ihr Accountname nicht mehr
gefiele. Mit der Option fühlte Mirash besonders mit.
