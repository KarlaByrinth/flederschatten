Zeitkick
========

Mirash bemerkte sehr wohl, wie Flammenfingers Blick auf ihm
ruhte, als as die Dramaturge verließ. Zeitkick
hatte Verständnis geäußert, wenn Mirash nicht gewollt hätte, und
gemeint, sie könnten sich ja vor der Dramaturge kurz über Pläne
unterhalten. Mirash war nicht gleich mitgegangen, sondern hatte
versprochen, zügig nachzukommen. Gerade, um die
Reaktion der Anwesenden zu beobachten, die sich dann nur
auf as und nicht vorwiegend auf Zeitkick beziehen würden. Flammenfingers
Gesicht wirkte unergründlich, der Mund vielleicht etwas streng, aber
das konnte auch einfach daran liegen, dass sey nicht lächelte. Ser Blick
streifte Mirashs jedes Mal, wenn as sich umdrehte, bis as die Tür hinter sich
schloss.

"Das hat ja nicht lange gedauert.", kommentierte Zeitkick. "Was
wolltest du noch?"

"Privatangelegenheit." Mirash drückte es auch deshalb so
direkt abwehrend aus, weil as interessiert war, wie Zeitkick
reagieren würde.

Zeitkick nickte lediglich. "Ich würde gern in die Zeitbasis gehen. Kannst
du einigermaßen fix entscheiden, ob du mitkommen möchtest oder andere
Pläne hast?"

Mirash überlegte. Eigentlich mochte as nicht so gern unter Druck
gesetzt werden, aber verstand auch, dass Zeitkick nun einmal Pläne
hatte. "Mein Grund, zurück in die Dramaturge zu gehen, wäre Zeittricks
kennen zu lernen. Würde ich alternativ solche, wenn ich mit dir käme, zeitnah
sehen?" As schmunzelte über die Anzahl an Zeitbezügen, die as in
der Kürze untergebracht hatte.

Zeitkick grinste. "Klar." Sie sortierte endlich die Locken unter den Maskenschnüren
zurecht, die die ganze Zeit eine leichte Wölbung oberhalb der
davon gebildet hatten. Es blies ein leichter Wind. Vielleicht hatte
sie es deshalb erst jetzt bemerkt. "Die Zeitbasis wird mit, nun ja, Zeittricks
geschützt. Ich kann dir einiges zeigen."

Mirash nickte. "Dann komme ich mit."

Zeitkick wandte sich zum Gehen und legte, wie Mirashs erste
Begleitung, ein zügiges Tempo vor, aber sie achtete darauf, dass
Mirash gut mitkam.

Sie verlangsamte das Tempo mit der Zeit, weil sie viele Schrägen und Brücken
hinaufstiegen und später auch Treppen. Mirash kannte Fork ausschließlich
aus Virtualitäten. As hatte die Metropole nie im Outernet besucht. Aber
ein paar Orte der Stadt, wie sie heute aussahen, kannte Mirash aus
Unterrichtseinheiten. Die Stadt war zentraler Treffpunkt für Friedensverhandlungen
gewesen, weil sie zum Beginn der Verhandlungen am ehesten neutraler
Grund mit vorhandener Infrastruktur gewesen
war. Sie war von Anfang an eine Stadt gewesen, an der
viele verschiedene Völker mitgebaut hatten. Sie war auf ein zentral
in Maerdha gelegenes Sumpfgebiet gebaut worden und statt eines
Straßennetzes am Boden bestand sie vor allem aus
mächtigen Brücken.

Mirash überraschten mehrere Dinge und as zögerte nicht, Fragen
dazu zu stellen. "Wir erreichen viel früher das obere Stockwerk
der Stadt, als ich vermutet hätte."

Zeitkick blickte as mit schmalem Lächeln an. "Wahrscheinlich hältst
du die Dramaturge für ein Bauwerk im untersten Stockwerk."

Mirash nickte zögerlich. "Darunter war Wasser. Eigentlich dachte ich
sogar, weil die Gegend unter Fork ein Überflutungsgebiet
ist, gäbe es keine Wohnungen und Häuser auf
Wasserhöhe, lediglich ein paar Schwimmhäuser oder Hausboote. Die
Dramaturge wirkte unrealistisch nah am Wasser. Und undicht."

"Bei einem relativ frühen Update wurde ein Drittel der Stadt in
dieser Virtualität für die Nixen unter Wasser gesetzt.", erklärte
Zeitkick schlicht.

Mirash musste die Antwort erstmal verarbeiten, bevor as die
zweite Frage formulierte. Das machte die Virtualität vielleicht
sehr interessant. Mirash mochte die Idee einer bekannten Stadt
in einem Spiel, die überwiegend unter Wasser stand. As erinnerte
sich auch daran, bei der Accounteinrichtung eine Auswahl gehabt
zu haben, ob as einen Fischschwanz haben wollte und darüber
ganz am Anfang eine Weile nachgedacht zu haben.

Sie erreichten den zentralen Pfeiler Forks. Die ursprüngliche
verzweigte Brücke, die Fork einmal gewesen war, bestand aus
mächtigen Pfeilern, in die Wohnungen gebaut waren, mit
inneren und äußeren Treppenhäusern und breiten, mächtigen Brücken, die sie
miteinander verbanden. Brücken, die wiederum von Häusern gesäumt waren. Einige
der Pfeiler waren mit dem Wachstum der Stadt verjüngt weiter in die
Höhe gebaut worden und der zentrale Pfeiler Forks war ein vertrauter Anblick. In ihm
lag im heutigen Fork der Hauptbahnhof und oben auf seine Spitze war
das Ratsgebäude gebaut worden, in dem die großen, ersten Friedensverhandlungen
stattgefunden hatten, -- das Deeskalat. "Die Zeitbasis ist nicht
im Deeskalat, oder doch?" Das war Mirashs zweite Frage gewesen.

"Ähm, doch." Zeitkick warf einen Seitenblick auf as. "Findest
du das irgendwie schlimm?"

"Ist das ein Statement?", fragte Mirash. As wusste noch nicht
so genau, was as davon halten sollte.

"In wiefern?", fragte Zeitkick.

"Es ist sozusagen das zentralste Gebäude in ganz Fork, oder
sogar in ganz Maerdha.", hielt
Mirash fest. As nahm den Hut ab, um die Krempe nicht im Sichtfeld
zu haben, als as nach oben blickte. Es war nicht mehr viel
zu steigen. Im Himmel darüber ruhten vier nicht allzu große
Zeppeline.

"Ist es. Und das ist nicht alles. Es handelt sich schließlich nicht
nur um ein Gebäude, sondern um einen ganzen gut bewachbaren
Hof.", bestätigte Zeitkick. "Die Basis ist perfekt für die Zeit-Fraktion. Wir
sind nicht so viele, aber wir haben viele wertvolle Ressourcen. Das gehört
zur Spielmechanik. Zeit ist am schwierigsten zu lernen. Deshalb lernen
es nicht so viele, aber wir werden mit der Zeit -- haha -- ziemlich
mächtig."

Mirash setzte den Zylinder wieder auf und geriet ins Grübeln. "Weil ihr
weniger seid, baut ihr zum Ausgleich zeitaufwendig etwas auf, das dann
wie eine Festung fungiert?"

Zeitkick nickte. "Es ist nicht so, als hätten nicht alle irgendwo
ihre Basen und Festungen. Aber die Zeitbasis ist schon am besten
geschützt aus diesem Grund. Gehen wir weiter?"

Obwohl es nur noch eine Treppenwindung war, war Mirash recht aus der
Puste, als sie an einem der Eingänge ankamen. Zeitkick wartete, bis as sich
erholt hatte. "Wenn du erst etwas gelevelt hast, wird es
weniger anstrengend.", versprach sie. "Und wenn es ganz schlimm
ist, haben wir auch Aufzüge."

Als Mirash wieder ruhiger atmete, fiel sain Blick auf das Schloss. Und
verstand, warum Zeitkick mit ihm vor der Tür stehen geblieben war. Es
war, nun, ein Zeitschloss, beschloss Mirash. Eine kreisrunde, durchsichtige
Scheibe war in Schlossnähe in die Tür eingelassen. Sie erinnerte an eine
Wählscheibe eines dieser im Outernet uralten Telefonanlagen, die wiederum
in diesem alten Parallel-Fork noch gar nicht erfunden sein mochten, nur glatter. Sie
hatte ein längliches Loch oder einen Schlitz, der etwa von der Mitte bis zum linken Rand
reichte. An einem Faden in der Mitte hinter der Scheibe hing ein Schlüssel
herab. Er konnte nicht vom Loch aus mit den Fingern erreicht werden, höchstens vielleicht
mit einem sehr geschickt gebogenen Draht. Mit der Innenseite der Scheibe
rechts vom Schlüssel war ein Brett verbunden. Wurde die Scheibe also gedreht, konnte
der Schlüssel auf dem Brett dorthin verschoben werden, wo jetzt das Loch war, aber dann wäre
das Loch woanders. Mirash nickte.

"Verstanden?", fragte Zeitkick, das Zeichen deutend.

"Ich glaube, schon.", antwortete Mirash.

"Willst du es selbst anfassen, oder soll ich auflösen?", fragte Zeitkick.

Mirash machte einen Schritt rückwärts und eine einladende Geste. As
überlegte, vielleicht später irgendwann, wenn niemand zusah, noch einmal
herzukommen. Und zu erleben, ob auch ohne Zeitkick an
sainer Seite an dieser Tür zu stehen so sicher war. As waren die vier Personen auf
den zwei von hier aus sichtbaren Türmen nicht entgangen, die entspannt zu ihnen
herunterblickten.

Zeitkick trat vor und ließ Mirash zusehen, als sie das Glas am Loch
eine gute Vierteldrehung im Uhrzeigersinn drehte. Der Schlüssel wurde, wie
erwartet, auf der Platte mitgeschoben, bis etwas über die Stelle hinaus, an
der eben noch das Loch gewesen war. Dann machte Zeitkick mit der anderen
Hand ein paar komplexe Gesten, bevor sie die Scheibe wieder zurückdrehte. Der
Schlüssel fiel wie in Zeitlupe, sodass Zeitkick ihn aus dem Schlitz entnehmen
konnte, als er daran vorbeikam. Die Schnur, an der er hing, war lang genug, dass
sie damit das Schloss der Tür erreichen konnte. Sie schloss die Tür auf und
steckte den Schlüssel zurück durch den Schlitz, wo er zurückschwang. Die Pendelbewegung
wurde von der Platte abgebremst.

Als Mirash Zeitkick durch die Tür folgte, hallte das bewegte Bild des
Schlüssels, der in Zeitlupe fiel, in sainem Kopf nach. "Wie sicher
ist diese Tür?"

"Es ist ein verhältnismäßig einfacher Zeit-Zauber.", antwortete Zeitkick. "Es
hat mich trotzdem etwa eine volle Woche Spielzeit gekostet, bis ich ihn für
den Schlüssel anwenden konnte. Verzwangzigfache die Zeit etwa für Personen, die
sich nicht für Zeit als ihr Element entschieden haben."

Mirash nickte langsam, während as sich die Umgebung ansah. Durch die Tür kamen
sie auf eine nicht überdachte Treppe, die in einen Hof führte. "Das ist viel Zeit."

Aber eigentlich war die Antwort in eine ganz andere Richtung gegangen, als
wohin Mirash gedacht hatte. As war nicht so ganz klar, warum as es für sich
behielt und nicht darauf hinwies, dass auch erhöhte Luftreibung
den Schlüssel zum langsamer Fallen
bringen könnte. Vielleicht, weil as sich noch nicht endgültig zwischen Reibung
und Zeit entschieden hatte. As lächelte, als ihm klar wurde, dass as sich bereits
so schnell auf diese beiden Elemente eingeschränkt hatte. "Ist der Schlüssel
aus Metall?", fragte as stattdessen.

Zeitkick grinste breit und schüttelte den Kopf. "Sehr stabiles Holz. Das sollte
für keine der anderen Gruppen manipulierbar sein." Sie war auf dem Hof, auf den
die Treppe führte, angekommen. "Aber es gibt durchaus irgendwelche Tricks, wie
Leute sich reinschummeln könnten. Es haben Personen schon mit Draht herumgespielt. Nur, sie
sind nicht weit gekommen. Sind dir die Wachen aufgefallen?"

Mirash nickte. Ihr Gespräch wurde davon unterbrochen, dass eine weitere Person über
den Hof auf sie zuschritt. Sie trug schlicht schwarze, eng anliegende
Kleidung mit einem schwarzen aufgestickten Schnörkelmuster, das glänzte. Mirash
lächelte. Das war Stil, den as sich vielleicht in etwa für sich ausgemalt hatte. Bis
as Zeitkick getroffen hatte. Spitzenrock mit Hosenträger und Oberhemd gab Mirash
schon sehr starke Gender-Euphorie-Vibes.

"Mirash, Holgem.", stellte Zeitkick sie einander vor. Holgem hatte wie
Zeitkick die Pronomen sie, ihr, ihr, sie.

Mirash runzelte die Stirn. "Ist es üblich, Personen für andere vorzustellen und
sie nicht selbst die Entscheidung fällen zu lassen?"

"In der Zeitbasis halten wir das eigentlich so.", antwortete Zeitkick. "Es
tut mir leid, ich hatte nicht daran gedacht, dass du noch gar nicht zu
Zeit gehörst."

Noch gar nicht, wiederholte Mirash in Gedanken. "Du scheinst dir
einigermaßen sicher zu sein, dass ich mir Zeit aussuchen würde."

"Zeitkick ist häufig ein bisschen voreilig.", sagte Holgem mit einem
Lächeln auf dem Gesicht und sehr gelassen. Sie strahlte auf Mirash
eine überraschende Ruhe aus. "Eigentlich sollte gerade heute keine Person
auf unser Gelände, die nicht zur Zeitfraktion gehört. Aber ich schätze, du
bist dir sehr sicher, dass Mirash keine Spionage betreibt?"

"Oh.", machte Zeitkick. "Ich bin heute ein bisschen durch den Wind. Die
Vorführung in der Dramaturge ist schlecht verlaufen."

"Ich verstehe." Holgem wirkte auf Mirash trotz der strengen Worte
ziemlich einfühlsam. Sie wandte sich ihm zu. "Einmal im Monat
sichern wir unsere Basis neu ab. In gut einer Stunde geht das
los. Bis dahin bist du hier sehr willkommen. Ich führe dich gern
im Hof und Eingangsbereich herum und erzähle ein bisschen. Und ab
dann, wenn du dich vorher für Zeit entscheiden solltest, aber
mach dir keinen Druck." Holgem zwinkerte mit einem Auge.

Mirash stimmte zu. Sie schritten gemütlich über den Hof um das
Deeskalat herum. Es war wirklich das Deeskalat, wie es vor einem
guten Jahrhundert ausgesehen haben mochte. Das war beeindruckend. Darum
herum war Raum für eine ummauerte Fläche mit Bäumen, etwas Wiese und
allerlei Pflastergestein. Die Mauer wurde von vier kleinen
Aussichtsplattformen unterbrochen, auf
denen jeweils zwei Wachen standen, die Mirash freundlich begrüßten. Auf
dem Weg von der zweiten zur dritten Aussichtsplattform erklärte Holgem: "Die
Wachenpaare bestehen immer aus Teams aus einer fortgeschrittenen
Zeit-Person und einer anfangenden. Wenn nicht viel los ist, können
sie dort miteinander trainieren. Wenn Mal wieder Gruppen aus den anderen
Fraktionen versuchen, die Zeitbasis zu ärgern, indem sie ein paar
Pfeile oder andere Geschosse darauf abschießen, halten die Fortgeschrittenen das Zeitschild
aufrecht, und die Anfangenden fegen die Geschosse mit einem Besen herunter, bevor
ihre Zeit wieder fortgesetzt wird."

"Ich habe bisher nur einen Besen gesehen.", stellte Mirash amüsiert fest. As
stellte sich durchaus vorübergehend witzig vor, Pfeile mit dem Besen aus der
Luft zu fegen.

"Mangel an Auszubildenden derzeit.", erklärte Holgem.

"Also würdet ihr euch sehr freuen, wenn ich Zeit wählte?" Mirash grinste
breiter.

Aber Holgem antwortete auf ähnliche Art, wie Zeitkick. "Es kommt drauf
an.", sagte sie. "Wir mögen ein eingespieltes Team mit einer guten
und stabilen, sozialen Gemeinschaft sein. Wenn du gut zu uns passt und
wir zu dir, dann herzlich gern."

"Macht ihr dann eine Art großes Kennenlernen?", fragte Mirash. "Habt
ihr dazu auch Zeitmagie oder sowas, womit das überhaupt bis in einer
Stunde klappen könnte?"

Holgem lachte auf. "Das war als Scherz gemeint vorhin.", sagte sie, gluckste
aber noch ein bisschen, bevor sie fortfuhr, "Du wirkst sympathisch. Und
auf den ersten Blick zuverlässig und als würdest du dich an Absprachen
halten. Und du sagtest ja auch schon, dass du viel Spielzeit investieren
möchtest. Ich würde mir da nicht so viele Sorgen machen. Aber an sich gehört
schon dazu, dass du vorher zu einer unserer offeneren Planungssitzungen
dazustößt und dich vorstellst, ein paar Fragen beantwortest, und ein
bisschen socialiced."

Mirash runzelte die Stirn und fing gleichzeitig vorsichtig zu
grinsen an. Es machte as neugierig, was hier in einer Stunde passieren
sollte. Einmal im Monat war auch unter dem Gesichtspunkt, dass as
viel Spielen wollte, eine lange Zeit. Mirash rief das Overlay auf, das
alle möglichen Einstellungen zum Spiel anzeigte deaktivierte, damit es
unauffälliger wäre, die Übertragung der Armbewegungen in die Virtualität mit einer
Geste und durchsuchte das Menü. As sah die anderen beiden noch. Sie
sahen Mirash mit an den Seiten natürlich hängenden Armen. As fand
relativ zügig die Option, sich eines der Elemente auszuwählen. As
zögerte nur ein paar Augenblicke, dachte dann einfach 'für das
Chaos' und wählte 'Zeit' aus. As wurde noch einmal daran erinnert, dass
die Entscheidung endgültig wäre, und kam nach erneutem Bestätigen
in ein Menü, um sich einen neuen Satz Klamotten auszuwählen. Das
war logisch, aber Mirash hatte es vergessen. As übernahm nach kurzem
Zögern den Vorschlag, die derzeitige Kleidung einfach in Schwarz
zu wählen. Erst zu spät sah as, dass das der zunächst einzige
Satz Kleidung wäre, den as besitzen würde, as nicht einfach etwas
anderes aussuchen könnte, sondern ab nun sich im Spiel um neue
Kleidung kümmern müsste.

"Ehem, du möchtest dich in unsere Gruppe hineinschummeln, also.", sagte
Holgem mit gespielt strenger Stimme.

"Dachte ich mir so." Mirash schloss das Menü und grinste breit. "Was
wollt ihr tun?"

"Nun ja, ich denke, dich in unsere Basis einführen.", meinte Holgem. Sie
wirkte, fand Mirash, nicht ganz so gelassen, wie as es sich vorgestellt
hätte. "Für dich gibt es kein Zurück. Für uns ist natürlich die beste
Option, zu versuchen, zusammenzuhalten."

Das klang durchaus logisch. Mirash spürte trotzdem dieses leichte und
durchaus erwartete Unbehagen im Bauch, als wäre as über irgendwelche
Grenzen gegangen. Aber auf der anderen Seite, soweit Mirash das Spiel
verstanden hatte, durften eben eigentlich alle Spielenden sich für jedes
Element entscheiden. Wenn die Zeit-Fraktion das nicht zulassen wollte, dann
war das vielleicht auch einfach deren Problem. Oder nicht?

"Ich jedenfalls freue mich!" Zeitkick wirkte auch so. Definitiv. "Dann
können wir miteinander Bogenschießen lernen. Wenn du magst, werde ich dich
sowas von ausbilden in allem möglichen!"

Mirash nickte und lupfte dabei den Hut. "Mit Vergnügen."

---

Holgem leitete die Führung weiter. Aber so richtig glücklich wirkte sie eine
ganze Weile nicht.

Im Innenhof vor dem Deeskalat spawnten mehr Personen von der Zeitfraktion. Zeitkick
stellte sie nun wieder einander vor. As wurde als neues Mitglied vorgestellt. Es
wunderten sich ein paar, dass sie noch nichts von Mirash mitbekommen hatten. Und
wie bei Mirash nicht unüblich, löste das Reaktionsverhalten darauf, dass as
etwas gegen ein Prinzip tat, Trotz in ihm aus, und das wiederum das
Selbstbewusstsein, gegen die Reaktionen anzustehen. Mirash war da vielleicht
ungewöhnlich. As verstand auch, warum viele so etwas verunsicherte. Und es
gab in mehr oder regelmäßigen Abständen immer wieder Momente, in denen as
sich durch und durch in Frage stellte, mit sich selbst und dem ganzen gegen
irgendwas anstehen nicht zurecht kam. Weil es ja teils auch aus Prinzip
war, wie, ein Spiel ohne Anleitung kennen lernen. Musste as das tun? Vielleicht
hatte es einen gewissen Aspekt Selbstverletzenden
Verhaltens, dass Mirash sich regelmäßig in solche Situationen begab, bewusst
Unbehagen bei anderen gegen sich auszulösen, indem as etwas gegen eine
unausgesprochene Regel tat.

Schließlich zogen sie in den Eingangsbereich des Deeskalats um, wo weitere
Personen spawnten und ein paar schon warteten. Mirash erklärte vielen
Personen einzeln, wie as an Spiele heranging. Es wurde
verschieden aufgefasst. Sie waren vielleicht 20 Leute, als keine neuen mehr
hinzustießen. Mirash versuchte, sich an den Rand der Gruppe zu wuseln, in
eine dunklere Nische nahe eines Treppenhauses, und fand
dort Zeitkick.

"Du hast echt Schneid.", murmelte sie ihm zu.

Mirash blickte sie skeptisch an.

"Ich meine, wirklich. Und ich meine das nicht böse oder sowas.", fügte
Zeitkick hinzu. Und dann nachdenklich: "Hm, okay, vielleicht hat es dich nicht
so viel Mut gekostet, wie es mich gekostet
hätte, weil du nicht weißt, dass du dich gerade vor
der zweitmächtigsten Person im Spiel gegen den üblichen Weg entschieden
hast."

"Holgem ist zweirmächtigste Person im ganzen Spiel?", fragte Mirash. Mit einem
belustigten Gefühl, das as versuchte zu unterdrücken. Die Gedanken, die as
dabei hatte waren: 'Umso besser, wenn so eine Geste tun, dann richtig!' und 'Ja, es
ist vielleicht klar, dass die mächtigsten Spielenden wohl zur Zeit-Fraktion
gehören, aber lässt sich das wirklich so ranken?'

"Ja, ist sie." Zeitkicks vermittelte Emotionen lagen irgendwo zwischen
Bewunderung und Unbehagen.

"Und wer ist mächtigste Person?" Mirash grinste innerlich, als as auffiel, dass
das vielleicht Mirashs erste gedankliche Reaktion hätte sein sollen.

"Flederschatten." Zeitkick klang dieses Mal vergleichsweise unemotional.

"Gehört Flederschatten auch zu Zeit?", fragte Mirash.

"Flederschatten ist abtrünnig.", fasste Zeitkick zusammen. "Also, ja, Flederschatten
hat Zeit gewählt, kämpft aber trotzdem gegen uns."

"Warum?", fragte Mirash.

"Gute Frage. So richtig weiß das niemand." Zeitkick zog sich noch weiter aus
der Menge zurück. "Flederschatten gehört quasi zum Spiel-Inventar, war schon
immer da. Vielleicht ist Flederschatten noch von so einer Art alten Schule. Oder
Flederschatten will noch mächtiger werden. Dieser Charakter ist so maßlos
overpowered und reich. Das ist die einzige Person im Spiel, vor der
die Zeitfraktion wirklich Angst hat."

"Hm.", machte Mirash. As folgte Zeitkick weiter von der Menge weg und lehnte
sich neben sie an eine Wand. "Wenn ihr vor keiner anderen Fraktion
Angst habt, was sagt das über euch aus?"

"Du verstehst nicht.", widersprach Zeitkick. "Natürlich können uns die
anderen Faktionen was anhaben. Also, schon, zugegeben, wir sind die stärkste
Gruppe. Aber die anderen hätten problemlos das Potenzial eine Gefahr für
uns darzustellen, wenn sie Struktur in ihre Fraktionen bringen würden. Gezielteres
Training, durchdachte Angriffspläne, fokussiertere Ausbildung, sowas. Ihnen ist
das nicht so wichtig. Vielleicht, weil wir auch so
für die anderen wiederum eine ähnlich kleine Gefahr darstellen, weil
wir uns nicht darauf konzentrieren, sie fertig zu machen, selbst, wenn wir
könnten, sondern fair zu spielen. Es geht ja in diesem Spiel eigentlich
darum, gegen die anderen Fraktionen zu kämpfen, und wir sind halt noch
sehr freundlich dafür, was wir könnten." Zeitkick machte eine kurze
Redepause. Sie wirkte nicht so komfortabel in der Nähe einer großen
Gruppe von Leuten, und Mirash konnte das gut nachvollziehen. "Flederschatten
hat mehr chaotic Villain-Charakter. Flederschatten arbeitet aus eigennützigem
Antrieb und möchte gezielt uns fertig machen. Wir wissen nicht wie sehr, oder
was Flederschattens Ziel dabei ist. Wir wissen nur, dass wir hier in
der Basis sicher sind, aber außerhalb droht uns immer die Gefahr, dass diese
Person wieder einen erfolgreichen Angriff auf uns fährt. Und wir wissen, dass
sie irgendetwas plant."

"Gruselig.", murmelte Mirash bloß. Und genoss das Gruseln. Flederschatten
war auch ein schöner Name für so einen Charakter.

Zeitkick wirkte unruhig. Ihr Blick ruhte nicht auf Mirash sondern wanderte
durch die Menge. Und schließlich, als diese gerade peak-laut war, murmelte
sie Mirash zu: "Komm mit."

Mirash folgte Zeitkick ohne zu zögern zwei Stockwerke tiefer
in einen geräumigen Flur. Zeitkick
ging sehr leise und Mirash passte sich in Tempo und Schleichstil an. Neben
einer zugeschweißten Tür hielten sie inne. Hier lehnte sich Zeitkick
wieder an die Wand.

"Du hast doch Zeit auch so fix ausgesucht, weil du wissen wolltest, was wir
vorhaben, richtig?", mutmaßte Zeitkick.

Mirash nickte. Was für eine Tür. Sie war aus starkem Metall, eingesetzt in
einen ebenso starken, im Gemäuer verankerten Metallrahmen, aber es hielten sie nicht
Angeln oder Schlösser, sondern sie war damit mit einer breiten, wulstigen
Schweißnaht fest verbunden.

"Ich weiß nicht, ob sie dir das gezeigt hätten, weil du so frisch
dabei bist. Aber ich mag dich und ich bewundere dich irgendwie. Ich
wollte es dir zeigen.", sagte Zeitkick leise. "Ich habe ein bisschen
Angst vor Ärger."

Mirash wandte den Blick von der Tür ab und Zeitkick zu. "Angst?"

"Kannst du dir vielleicht nicht vorstellen, so furchtlos, wie
du bist." Zeitkick wirkte frustriert und vielleicht ein bisschen
angepisst.

"Es tut mir leid, ich wollte nicht sagen, dass es was Schlimmes
wäre, dass du Angst hast. Und ich kenne Angst durchaus, so ist
das nicht.", sagte Mirash leise. "Okay, ich habe tatsächlich
weniger und seltener Angst, als die meisten. Ich fand eher spontan
schlimm, dass es einen Grund gibt, also, dass du vor etwas Angst
haben musst. Dass so eine Sache wie,
du zeigst mir in einem Spiel etwas, Angst vor deiner eigenen
Gruppe auslöst."

"So passiert bei sozialen Dingen nun mal.", verteidigte sich Zeitkick. "Ich
habe halt immer Angst, nicht zu gefallen. Nicht dazu zu gehören.
Nicht gemocht zu werden. Nicht gut genug zu sein."

Mirash spürte den Drang, Zeitkick sofort zu sagen, dass as sie
mochte, oder sie in den Arm zu nehmen. Aber irgendetwas in
ihm rief zu Vorsicht auf. Eine Art unscharfer Erinnerung. Egal, Zeitkick
brauchte gerade Rückversicherung. Und Mirash fühlte tatsächlich
Sympathie. "Ich mag dich bisher."

Zeitkick lächelte milde. "Ich dich auch.", sagte sie. "Aber
das wird gegen jahrelange Anxiety halt nicht mal eben helfen."

Mirash nickte. "Ich weiß. Es tut mir leid."

"Zurück zur Tür!", wechselte Zeitkick das Thema. "Wir
schweißen sie einmal im Monat auf, halten sie eine halbe
Stunde offen, schwer bewacht -- deshalb sind gerade so
viele von uns da --, und schweißen sie wieder zu. Verstehst
du warum?"

Mirash hatte sich bereits Gedanken gemacht, während sie sich
über anderes unterhalten hatten. "Ihr reist in der Zeit
zurück, wo sie offen war, tretet hindurch und reist dann
wieder vor?"

"Fast." Zeitkick grinste und schüttelte den Kopf. "Personen
können in der Zeit nicht zurückreisen. Das geht nicht, weil, nunja
Spieltechnik. Dann hätte dich ja, wenn du zeitreist, eventuell
eine andere Person in ihrer Vergangenheit im Nachhinein gesehen haben
müssen, oder das, was du geändert hast hätte wahrgenommen worden
sein müssen, und das kann das Spiel nicht
korrigieren."

"Das Spiel könnte zusehen, dass zurückreisende Leute nicht gesehen
werden, und dass sie bei Veränderung wieder aufräumen müssen.", überlegte
Mirash, aber überzeugte sich selbst nicht. "Hm. Das hätte sehr
eingeschränkte Anwendungsfälle."

Zeitkick freute sich vor sich hin. Mirash mutmaßte, dass sie
sehr gern lehrte. "Wir reisen jedenfalls die Tür zurück.", sagte
sie. "Das ist ein sehr mächtiger Zeitzauber, den ich selbst
nicht beherrsche. Ich bin also auf andere angewiesen, wenn ich
reinwill."

"Wird der Zauber noch mächtiger, je weiter sie gereist werden muss?", fragte
Mirash.

"Ja." Zeitkick grinste. "Dinge in die Vergangenheit reisen kostet mehr, je
größer der Gegenstand oder Ausschnitt eines Gegenstands ist und
je weiter in die Vergangenheit er gereist wird. Daher wird sie einmal
im Monat neu eingeschweißt."

Sie beendeten ihr Gespräch, als sich andere aus der Zeitfraktion mit
den Utensilien fürs Schweißen näherten. Mirash erkannte sehr wohl in den Gesichtern, dass
manche nicht so glücklich darüber waren, dass sie hier standen.
